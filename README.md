# Micro-Frontend application
You can find `back-end application` for this project here:
https://gitlab.com/dino10bit/business-entry-back-end-app

`Demo:` https://youtu.be/GTKwwKudJ7g

This project was build with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.1.

## Development server

1. `$ yarn`
2. `$ ng serve mf1`
3. `$ ng serve host`

Navigate to `http://localhost:5000/`
