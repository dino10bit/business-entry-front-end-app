import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddBusinessRoutingModule } from './add-business-routing.module';
import { AddBusinessComponent } from './add-business.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AddBusinessComponent],
  imports: [CommonModule, AddBusinessRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
})
export class AddBusinessModule {}
