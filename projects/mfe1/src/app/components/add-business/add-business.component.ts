import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Business } from '../../../../../host/src/app/models/business.model';
import { BusinessService } from '../../services/business.service';
import { OpeningDays } from "../../../../../host/src/app/models/days.model";
import { OpeningTimes } from "../../../../../host/src/app/models/times.model";

@Component({
  selector: 'app-settings',
  templateUrl: './add-business.component.html',
  styleUrls: ['./add-business.component.scss'],
})
export class AddBusinessComponent implements OnInit {
  addForm: FormGroup | any;
  rows: FormArray | any;

  business: Business = {
    name: '',
    address: '',
  };
  submitted = false;

  constructor(private fb: FormBuilder, private businessService: BusinessService) {
    this.addForm = this.fb.group({
      items: [null, Validators.required],
    });

    this.rows = this.fb.array([]);
  }

  ngOnInit() {
    this.addForm.addControl('rows', this.rows);
    this.onAddRow();
  }

  onAddRow() {
    this.rows.push(this.createItemFormGroup());
  }

  onRemoveRow(rowIndex: number) {
    this.rows.removeAt(rowIndex);
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      dayOfWeek: null,
      openingHour: null,
      closingHour: null,
      type: null,
    });
  }

  groupArrayByKey(inputArray: any, key: string, removeKey = false, outputType = {}): OpeningTimes {
    return inputArray.reduce(
      (previous: any, current: any) => {
        // Get the current value that matches the input key and remove the key value for it.
        const { [key]: keyValue } = current;
        // remove the key if option is set
        removeKey && keyValue && delete current[key];
        // If there is already an array for the user provided key use it else default to an empty array.
        const { [keyValue]: reducedValue = [] } = previous;

        // Create a new object and return that merges the previous with the current object
        return Object.assign(previous, {
          [keyValue]: reducedValue.concat(current),
        });
      },
      // Replace the object here to an array to change output object to an array
      outputType,
    );
  }

  saveBusiness(): void {
    const openingDays = this.groupArrayByKey(this.rows.value, 'dayOfWeek', true);

    const openingDaysTransformed: OpeningDays[] = [];

    for (const [key, value] of Object.entries(openingDays)) {
      openingDaysTransformed.push({
        dayOfWeek: key.toUpperCase(),
        openingTimes: value,
      });
    }

    const data: Business = {
      name: this.business.name,
      address: this.business.address,
      openingDays: openingDaysTransformed,
    };

    this.businessService.create(data).subscribe(
      (response) => {
        console.log(response);
        this.submitted = true;
      },
      (error) => {
        console.log(error);
      },
    );
  }

  newBusiness(): void {
    this.submitted = false;
    this.business = {
      name: '',
      address: '',
    };
  }
}
