import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EnvironmentService } from "../../../../host/src/app/services/environment.service";
import { Business } from "../../../../host/src/app/models/business.model";

@Injectable({
  providedIn: 'root',
})
export class BusinessService {
  apiUrl: string;

  constructor(private http: HttpClient, private environmentService: EnvironmentService) {
    this.apiUrl = this.environmentService.getEnvConfig().apiUrl;
  }

  create(data: Business): Observable<any> {
    return this.http.post(this.apiUrl, data);
  }
}
