import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Business } from '../models/business.model';
import { EnvironmentService } from './environment.service';

@Injectable({
  providedIn: 'root',
})
export class BusinessService {
  apiUrl: string;

  constructor(private http: HttpClient, private environmentService: EnvironmentService) {
    this.apiUrl = this.environmentService.getEnvConfig().apiUrl;
  }

  getAll(): Observable<Business[]> {
    return this.http.get<Business[]>(`${this.apiUrl}/all`);
  }

  get(id: string): Observable<Business> {
    return this.http.get(`${this.apiUrl}/${id}`);
  }

  update(data: Business): Observable<Business> {
    return this.http.put(this.apiUrl, data);
  }

  delete(id: number | undefined): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.apiUrl);
  }

  findByName(name: string): Observable<Business[]> {
    return this.http.get<Business[]>(`${this.apiUrl}/all?name=${name}`);
  }
}
