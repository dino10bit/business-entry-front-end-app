import { OpeningDays } from './days.model';

export class Business {
  id?: number;
  name?: string;
  address?: string;
  openingDays?: OpeningDays[];
}
