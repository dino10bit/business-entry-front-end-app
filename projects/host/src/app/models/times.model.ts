export class OpeningTimes {
  openingHour?: string;
  closingHour?: string;
  type?: string;
}
