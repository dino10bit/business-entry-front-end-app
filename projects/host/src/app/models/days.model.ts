import { OpeningTimes } from './times.model';

export class OpeningDays {
  dayOfWeek?: string;
  openingTimes?: OpeningTimes[];
}
