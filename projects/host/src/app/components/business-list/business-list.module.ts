import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessListRoutingModule } from './business-list-routing.module';
import { BusinessListComponent } from './business-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [BusinessListComponent],
  imports: [CommonModule, BusinessListRoutingModule, FormsModule],
})
export class BusinessListModule {}
