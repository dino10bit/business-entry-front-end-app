import { Component, OnInit } from '@angular/core';
import { Business } from '../../models/business.model';
import { BusinessService } from '../../services/business.service';
import { OpeningDays } from '../../models/days.model';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.component.html',
  styleUrls: ['./business-list.component.scss'],
})
export class BusinessListComponent implements OnInit {
  businesses?: Business[];
  currentBusiness: Business = { address: '', name: '' };
  currentIndex = -1;
  name = '';

  constructor(private businessService: BusinessService) {}

  ngOnInit(): void {
    this.retrieveBusiness();
  }

  retrieveBusiness(): void {
    this.businessService.getAll().subscribe(
      (data) => {
        this.businesses = this.transformResult(data);
      },
      (error) => {
        console.log(error);
      },
    );
  }

  transformResult(data: Business[]): Business[] {
    data.forEach((element: Business) => {
      const groupedData: OpeningDays[] | undefined = [];
      element.openingDays?.forEach((openingTimes: OpeningDays) => this.groupByOpeningTime(openingTimes, groupedData));
      element.openingDays = groupedData;
    });

    return data;
  }

  groupByOpeningTime(data: OpeningDays, groupedData: OpeningDays[] = []): null | number {
    if (groupedData.length === 0) {
      return groupedData.push(data);
    }

    let found = false;

    groupedData.forEach((value: OpeningDays, index: number) => {
      if (JSON.stringify(value.openingTimes) === JSON.stringify(data.openingTimes)) {
        groupedData[index].dayOfWeek += ' - ' + data.dayOfWeek;
        found = true;
      }
    });

    return found ? null : groupedData.push(data);
  }

  refreshList(): void {
    this.retrieveBusiness();
    this.currentBusiness = { address: '', name: '' };
    this.currentIndex = -1;
  }

  setActiveBusiness(business: Business, index: number): void {
    this.currentBusiness = business;
    this.currentIndex = index;
  }

  removeAllBusiness(): void {
    this.businessService.deleteAll().subscribe(
      (response) => {
        console.log(response);
        this.refreshList();
      },
      (error) => {
        console.log(error);
      },
    );
  }

  searchName(): void {
    this.currentBusiness = { address: '', name: '' };
    this.currentIndex = -1;

    this.businessService.findByName(this.name).subscribe(
      (data) => {
        this.businesses = this.transformResult(data);
      },
      (error) => {
        console.log(error);
      },
    );
  }
}
