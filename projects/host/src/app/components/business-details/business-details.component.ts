import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Business } from '../../models/business.model';
import { BusinessService } from '../../services/business.service';

@Component({
  selector: 'app-business-details',
  templateUrl: './business-details.component.html',
  styleUrls: ['./business-details.component.css'],
})
export class BusinessDetailsComponent implements OnInit {
  currentBusiness: Business = {
    name: '',
    address: '',
  };
  message = '';

  constructor(private businessService: BusinessService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.message = '';
    this.getBusiness(this.route.snapshot.params.id);
  }

  getBusiness(id: string): void {
    this.businessService.get(id).subscribe(
      (data) => {
        this.currentBusiness = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      },
    );
  }

  updateBusiness(): void {
    this.message = '';

    this.businessService.update(this.currentBusiness).subscribe(
      (response) => {
        console.log(response);
        this.message = 'This business was updated successfully!';
      },
      (error) => {
        console.log(error);
      },
    );
  }

  deleteBusiness(): void {
    this.businessService.delete(this.currentBusiness.id).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(['/business-list']);
      },
      (error) => {
        console.log(error);
      },
    );
  }
}
