import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusinessDetailsComponent } from './components/business-details/business-details.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/business-list',
    pathMatch: 'full',
  },
  { path: 'business/:id', component: BusinessDetailsComponent },
  {
    path: 'business-list',
    loadChildren: () => import('./components/business-list/business-list.module').then((m) => m.BusinessListModule),
  },
  {
    path: 'add-business',
    loadChildren: () => import('mfe1/AddBusinessModule').then((m) => m.AddBusinessModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
